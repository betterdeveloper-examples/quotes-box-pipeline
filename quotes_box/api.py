from flask_restless.manager import APIManager
from quotes_box.model import db, Quote, Category


class QuotesBoxApi():

    def __init__(self, app):
        self.api = APIManager(app, flask_sqlalchemy_db=db)
        self._create_api()

    def _create_api(self):
        self.api.create_api(Quote, methods=['GET', 'POST', 'PUT', 'DELETE'])
        self.api.create_api(Category, methods=['GET', 'POST', 'PUT', 'DELETE'], include_columns=['id', 'name'])
