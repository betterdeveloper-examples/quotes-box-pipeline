import os
import tempfile


class Config:

    LOCAL_DATABASE_FILE = 'sqlite:///{0}/quotes-box.db'.format(tempfile.gettempdir())
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL', LOCAL_DATABASE_FILE)
    SECRET_KEY = '123456'
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class Production(Config):
    """
    Configurações para ambiente de Produção.
    """
    pass


class Development(Config):
    """
    Configurações para ambiente de Desenvolvimento.
    """
    DEBUG = True


class Testing(Development):
    """
    Configurações para ambiente de teste.
    """
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///:memory:'
