from flask_admin import Admin
from flask_admin.contrib.sqla.view import ModelView

from quotes_box.model import db, Quote, Category


class QuotesBoxAdminView():

    def __init__(self, app):
        self.view = Admin(app, name='Quotes Box', template_mode='bootstrap3')
        self._create_view()

    def _create_view(self):
        self.view.add_view(ModelView(Quote, db.session))
        self.view.add_view(CategoryView(Category, db.session))


class CategoryView(ModelView):
    form_excluded_columns = ['quotes']
