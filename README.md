#### Projeto de exemplo para a apresentação "Do código à produção (Mundo Python)". [Clique aqui para ver!](https://pt.slideshare.net/betterdeveloper)

## Finalidade

Esse projeto visa possibilitar o uso de Pipeline para partir da implementação de código até deploy em produção em poucos passos
e com processos automatizados de atualização de ambientes onde a aplicação é deployada.

## O que este projeto não é

Não é foco deste projeto aplicar as melhores práticas de desenvolvimento de software, embora tenha-se buscado aplicar um conjunto mínimo de organização.
Há outros projetos com intuito de mostrar boas práticas de desenvolvimento. Acesse os repositórios da Better Developer no: [GitLab](gitlab.com/betterdeveloper-example) ou [GitHub](github.com/betterdeveloper-example)

## Da solução

O projeto implementa um sistema para CRUD de Citações (Quotes) através de um painel administrativo.
Também está implementado uma API em REST para consulta e CRUD das citações para que outras views possam ser criados usando a API.

Link de acesso em produção: https://quotes-box-pipeline.herokuapp.com/admin

## Linguagem e Tecnologias Utilizadas 

O projeto está implementado na linguagem Python na versão 3.5.
Foram utilizados os frameworks e biliotecas abaixo:
- [SQLAlchemy](http://www.sqlalchemy.org/)
- [Alembic](https://pypi.python.org/pypi/alembic)
- [Flask](http://flask.pocoo.org/)
- [Flask-Cors](https://flask-cors.readthedocs.io/en/latest/)
- [Flask-SqlAlchemy](http://flask.pocoo.org/docs/0.11/patterns/sqlalchemy/#)
- [Flask-Restless](https://flask-restless.readthedocs.io/en/stable/)
- [Flask-Admin](https://flask-admin.readthedocs.io/en/latest/)
- [Flask-Migrate]()https://flask-migrate.readthedocs.io/en/latest/)

O projeto usar Make para processo de builds do fonte. Abaixo principais targets.
- all: executa os testes e convenção de código.
- run: para rodar o projeto localmente.
- db-migrate: para gerar script de migração em caso de mudanças da base de dados.
- db-upgrade: para aplicar as mudanças da base de dados.

O projeto constrói uma imagem Docker e esta é armazenada no registry do GitLab.

O deploy é feito em dois ambiente, um Staging e um Production na Cloud Heroku.