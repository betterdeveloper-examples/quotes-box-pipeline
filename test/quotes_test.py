import json
import unittest
from http import HTTPStatus

from quotes_box.app import create_app


class QuoteCrudTest(unittest.TestCase):
    """
    Test the Quote API.
    """

    def setUp(self):
        app = create_app("quotes_box.config.Testing")
        self.app = app.test_client()
        self.app.testing = True
        self.headers = [('Content-Type', 'application/json')]

        category_response = self.app.post('/api/category', headers=self.headers, data=json.dumps({"name": "filosofia"}))
        self.category_json = json.loads(category_response.data.decode('utf-8'))

    def test_get_all_quotes(self):
        # GIVEN
        quote = {"phrase": "O Mundo eh uma bola", "author": "Joao Souza"}
        self.app.post('/api/quote', headers=self.headers, data=json.dumps(quote))

        # WHEN
        response = self.app.get('/api/quote')
        response_json = json.loads(response.data.decode('utf-8'))

        # THEN
        self.assertEqual(1, len(response_json['objects']))

    def test_should_get_quote_by_id(self):
        # GIVEN
        quote = {"phrase": "O Mundo eh uma bola", "author": "Joao Souza", "category_id": self.category_json['id']}
        self.app.post('/api/quote', headers=self.headers, data=json.dumps(quote))

        # WHEN
        response = self.app.get('/api/quote/1')
        response_json = json.loads(response.data.decode('utf-8'))

        # THEN
        self.assertEqual('O Mundo eh uma bola', response_json['phrase'])
        category_json = response_json['category']
        self.assertEqual('filosofia', category_json['name'])

    def test_not_get_quote_by_nonexistent_id(self):
        # WHEN
        response = self.app.get('/api/quote/1')

        # THEN
        self.assertEqual(HTTPStatus.NOT_FOUND, response.status_code)

    def test_should_create_quote(self):
        # GIVEN
        quote = {"phrase": "O Mundo eh uma bola", "author": "Joao Souza"}

        # WHEN
        self.app.post('/api/quote', headers=self.headers, data=json.dumps(quote))

        response = self.app.get('/api/quote/1')
        response_json = json.loads(response.data.decode('utf-8'))

        # THEN
        self.assertEqual('O Mundo eh uma bola', response_json['phrase'])

    def test_should_not_create_quote_without_data(self):
        # GIVEN

        # WHEN
        response = self.app.post('/api/quote',
                                 headers=self.headers, data=json.dumps({}))

        # THEN
        self.assertEqual(response.status_code, HTTPStatus.BAD_REQUEST)

    def test_should_delete_quote(self):
        expect = HTTPStatus.NO_CONTENT

        # GIVEN
        quote = {"phrase": "O Mundo eh uma bola", "author": "Joao Souza"}
        self.app.post('/api/quote', headers=self.headers, data=json.dumps(quote))

        # WHEN
        response = self.app.delete('/api/quote/1')

        # THEN
        self.assertEqual(response.status_code, expect)

    def test_should_not_delete_quote_by_nonexistent_code(self):
        # GIVEN

        # WHEN
        response = self.app.delete('/api/uote/1')

        # THEN
        self.assertEqual(response.status_code, HTTPStatus.NOT_FOUND)

    def test_should_update_quote(self):
        # GIVEN
        quote = {"phrase": "O Mundo eh uma bola", "author": "Joao Souza"}
        self.app.post('/api/quote', headers=self.headers, data=json.dumps(quote))

        # WHEN
        self.app.put('/api/quote/1', headers=self.headers, data=json.dumps(
            {'phrase': 'O mundo eh um quadrado', "author": "Jose Souza"}))
        response = self.app.get('/api/quote/1')
        response_json = json.loads(response.data.decode('utf-8'))

        # THEN
        self.assertEqual('O mundo eh um quadrado', response_json['phrase'])

    def test_should_not_update_quote_without_data(self):
        # GIVEN
        quote = {"phrase": "O Mundo eh uma bola", "author": "Joao Souza"}
        self.app.post('/api/quote', headers=self.headers, data=json.dumps(quote))

        # WHEN
        response = self.app.put('/api/quote/1',
                                headers=self.headers, data=json.dumps({}))

        # THEN
        self.assertEqual(HTTPStatus.OK, response.status_code)

    def test_should_not_update_quote_with_nonexistent_code(self):
        # GIVEN
        data = {'phrase': 'O mundo eh um quadrado', "author": "Jose Souza"}
        # WHEN
        response = self.app.put('/api/quote/1',
                                headers=self.headers, data=json.dumps(data))

        # THEN
        self.assertEqual(HTTPStatus.NOT_FOUND, response.status_code)
