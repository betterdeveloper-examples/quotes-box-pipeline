import os

from quotes_box.app import create_app

app = create_app('quotes_box.config.' + os.getenv("APP_ENV", 'Production'))

if __name__ == "__main__":
    app.run(host='0.0.0.0')
