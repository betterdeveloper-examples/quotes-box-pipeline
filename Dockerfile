FROM betterdeveloper/quotes-box-pipeline
 
# Set environment variables
#ENV PORT=80
#EXPOSE 80

# Copy files to app directory
COPY . /app

# Set working directory
WORKDIR /app

ENV HOME=/app
ENV PATH=/app/:$PATH
ENV PYTHONPATH=/app

# Install dependencies requirements
RUN pip install --user -r requirements.txt && \
    chmod 775 -R /app run.sh

# Run the app
CMD ./run.sh